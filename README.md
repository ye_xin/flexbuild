## Flexbuild Overview
Flexbuild is a component-oriented build framework and integrated platform
with capabilities of flexible, ease-to-use, scalable system build and
distro installation. With flex-builder CLI, users can build various
components (linux, u-boot, uefi, rcw, atf and miscellaneous custom
userspace applications) and distro userland to generate composite
firmware, hybrid rootfs with customable userland.


## Supported Arch
```
ARM32(LE), ARM64(LE), ARM32(BE)
ARM64(BE), PPC32(BE), PPC64(BE)
```


## Supported Distros and Platforms
- Supported Distro on Layerscape platforms
```
  - Support Ubuntu (default for LSDK)
  Example:
  $ flex-builder -i autobuild -a arm64
  separate build commands:
  $ flex-builder -i mkrfs -r ubuntu -a arm64
  $ flex-builder -i mkfw -m ls1046ardb -b sd
  $ flex-builder -c apps -a arm64
  $ flex-builder -i mkbootpartition -a arm64
  $ flex-builder -i merge-component -a arm64
  $ flex-builder -i compressrfs -a arm64
  $ cd build/images
  $ flex-installer -b bootpartition_LS_arm64_lts_4.14.tgz -r rootfs_ubuntu_bionic_LS_arm64.tgz -f firmware_ls1046ardb_uboot_sdboot.img -d /dev/sdx

  - Support Debian (optional)
  Example:
  $ flex-builder -i mkrfs -r debian:tiny:stretch -a arm64

  - Support CentOS (optional)
  Example:
  $ flex-builder -i mkrfs -r centos -a arm64
  $ flex-builder -i mkbootpartition -a arm64
  $ flex-builder -i merge-component -r centos -a arm64
  $ flex-builder -i compressrfs -r centos -a arm64
  $ cd build/images
  $ flex-installer -b bootpartition_LS_arm64_lts_4.14.tgz -r rootfs_centos_7.6.1810_arm64.tgz -d /dev/sdx

  - Support Android (optional)
  Example:
  $ flex-builder -i mkrfs -r android -p layerscape -a arm64
  $ flex-installer -b bootpartition_LS_arm64_lts_4.14.tgz -r rootfs_android_v7.1.2_system.img -d /dev/mmcblk0

  - Support custom Buildroot-based tiny distro (optional)
  Example:
  $ flex-builder -i mkrfs -r buildroot:tiny -a arm64
  $ flex-builder -i mkrfs -r buildroot:custom -a arm64
  $ flex-builder -i mkrfs -r buildroot:moderate -a ppc64
  $ flex-installer -b bootpartition_LS_arm64_lts_4.14.tgz -r rootfs_buildroot_arm64_tiny.tgz -d /dev/mmcblk0

  Supported Layerscape platforms: ls1012ardb, ls1012afrwy, ls1021atwr, ls1043ardb, ls1046ardb, ls1088ardb, ls2088ardb,
                                  lx2160ardb, ls1043aqds, ls1046aqds, ls1088aqds, ls2088aqds, lx2160aqds, etc
  Supported QorIQ PPC platforms:  t1024rdb, t2080rdb, t4240rdb, etc
```


- Supported Distro on i.MX platforms
```
  -Support Android
  Example:
  $ flex-builder -i mkrfs -r android -p imx6 -a arm32
  $ flex-builder -i mkrfs -r android -p imx8 -a arm64
  $ flex-builder -i autobuild -a arm64 -p imx

  - Support Ubuntu, Debian, CentOS (optional)
  Example:
  $ flex-builder -c linux -p imx -a arm64
  $ flex-builder -c linux -p imx -a arm32
  $ flex-builder -c uboot -m mx8mqevk
  $ flex-builder -i mkfw  -m mx6qsabresd
  $ flex-builder -i mkrfs -r ubuntu -p imx -a arm64
  $ flex-builder -i mkrfs -r debian -p imx -a arm64
  $ flex-builder -i mkrfs -r centos -p imx -a arm64

  - Support customizable Buildroot-based tiny distro (optional)
  $ flex-builder -i mkrfs -r buildroot:custom -a arm32
  $ flex-builder -i mkrfs -r buildroot:tiny -a arm64

  Supported i.MX platforms: mx6qsabresd, mx8mqevk, etc
```

Users can choose the appropriate userland from various Ubuntu, Debian, CentOS,
Android or Buildroot-based tiny distro to adapt the needs in practic use case.


## Build Environment
- Cross-build on Ubuntu x86 host
- Native-build on Ubuntu ARM board
- Build in Docker hosted on any machine


## More info
See docs/flexbuild_usage.txt and docs/lsdk_build_install.txt for detailed information.
